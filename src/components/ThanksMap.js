import React from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';
import 'leaflet-routing-machine';
import "../css/ThanksMap.css";


// map container aanmaken
const Wrapper = styled.div`
    width: ${props => props.width};
    height: ${props => props.height};
`;

const startMarker = L.icon({
    iconUrl: 'images/start.png',
    iconSize:     [25, 25],
    shadowSize:   [50, 64],
    iconAnchor:   [11, 20],
    shadowAnchor: [4, 62],
    popupAnchor:  [-3, -76]
});

const destinationMarker = L.icon({
    iconUrl: 'images/destination.png',
    iconSize:     [30, 30],
    shadowSize:   [50, 64],
    iconAnchor:   [11, 20],
    shadowAnchor: [4, 62],
    popupAnchor:  [-3, -76]
});

export default class Map extends React.Component {

    // overkoepelende map functie
    componentDidMount() {

        // map aanmaken
        this.map = L.map('map', {
            minZoom: 13,
            maxZoom: 18
        }).setView([52.131097, 4.645999], 1);

        // map style toevoegen aan map
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        // array met het start adres en bestemming adres van een bestelling
        // let addresses = ["52.127126,4.651042", "52.139876,4.651394"];

        // route weergeven
        L.Routing.control({
            waypoints: [
                L.latLng(52.127126,4.651042),
                L.latLng(52.139876,4.651394)
            ],
            show: false,
            addWaypoints: false,
            draggableWaypoints: false,
            createMarker: function(i, wp, nWps) {
                if (i === 0) {
                    return L.marker(wp.latLng, {icon: startMarker });
                } else if (i === nWps -1) {
                    return L.marker(wp.latLng, {icon: destinationMarker });
                } else {
                    return L.marker(wp.latLng, {icon: destinationMarker });
                }
            }
        }).addTo(this.map);
    }

    render() {
        return <Wrapper width="100%" height="25vh" id="map" />
    }
}
