import React from 'react';
import Header from '../Header';
import '../../css/ondernemingregister.css';

const Register = () => {
    return (
        <>
            <Header title={"Registreer"} />
            <form>
                <section className="onderneming-naam">
                    <label htmlFor="onderneming-naam" className="onderneming-naam__label">Ondernemings naam</label>
                    <input type="text" id="onderneming-naam" className="onderneming-naam__naam-input" placeholder="Naam" />
                </section>
                <section className="onderneming-type">
                    <label htmlFor="onderneming-type" className="onderneming-type__label">Type onderneming</label>
                    <select id="onderneming-type" className="onderneming-type__type-input">
                        <option value="bar">Bar</option>
                        <option value="cafe">Cafe</option>
                        <option value="restaurant">Restaurant</option>
                    </select>
                </section>
                <section className="onderneming-email">
                    <label htmlFor="onderneming-email" className="onderneming-email__label">Email</label>
                    <input type="email"id="onderneming-email"className="onderneming-email__input" placeholder="E-mail" />
                </section>
                <section className="onderneming-submit">
                    <button type="submit" className="onderneming-submit__button">Volgende</button>
                </section>
            </form>
        </>
    );
}

export default Register;
