import React, { Component } from 'react';
import "../css/Addproduct.css";
import axios from 'axios';



class Addproduct extends Component{
    
    constructor(props) {
        super(props);
        this.state ={
            name: '',
            price: '',
            description: ''

        }
        
    }
    
    onChangeValue = (event) =>{
        this.setState({ [event.target.name]: event.target.value });
    }


    onSubmit = (event) => {
        event.preventDefault();

        const product = {
            name: this.state.name,
            price: this.state.price,
            description: this.state.description
        }

        const BASE_URL = 'http://127.0.0.1:8000/api/auth/products';

        axios.post(BASE_URL, product,{
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
        })
        .then(res => {
            window.location = "/restaurant"
        })
        .catch(error => {
            console.log(error.response.data.errors);
        });
    }
    
    

    render() {
        return(
            <section>
                <form onSubmit={this.onSubmit} className="addProducts-form">
                    <input type="text" className="addProducts-form__input 1" placeholder="Naam van Product" name="name" value={this.state.name} onChange={this.onChangeValue} required/>
                    <input type="number" min="0" className="addProducts-form__input 2" placeholder="Prijs van Product" name="price" value={this.state.price} onChange={this.onChangeValue} required/>
                    <textarea className="addProducts-form__textarea" placeholder="Beschrijving" name="description" value={this.state.description} onChange={this.onChangeValue} />
                    <input type="file" />
                    <button className="addProducts-form__button">Opslaan</button>
                </form>
            </section>
        );
    }
}
export default Addproduct;