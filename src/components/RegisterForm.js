import React from 'react';
import '../css/Register.css';
import '../css/RegisterForm.css';
import axios from 'axios';

// form

class RegisterForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            password: '',
            passwordConfirmation: '',
            email: '',
            address: '',
            role: ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });         
    }

    onSubmit(e) {
        e.preventDefault();
    
        axios.defaults.xsrfCookieName = "csrftoken"; 
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
    
        const user = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password,
            password_confirmation: this.state.passwordConfirmation,
            address: this.state.address,
            role: this.state.role
        };

        axios.post('http://localhost:8000/api/auth/signup', user,
        {
            headers: {'Content-Type': 'application/json'}
        })
        .then(res => {
            window.location = "/login"     
        })
        .catch(error => {
            
            const passwordConfirmationError = document.getElementById('js--passwordConfirmationError');
            const emailError = document.getElementById('js--emailError');

            const errorMessage = error.response.data.errors;
            
            passwordConfirmationError.innerHTML = errorMessage.password;
            emailError.innerHTML = errorMessage.email;

            if (!errorMessage.password) {
                passwordConfirmationError.innerHTML = '';
            }
            if (!errorMessage.email) {
                emailError.innerHTML = '';
            }
        });
    }

    render() {
        return (
            <form className="register__container__content" onSubmit={this.onSubmit}>
                <section className="register__container__content__article__section__1">
                    <h2>Naam</h2>
                    <input type="text" name="name" value={this.state.name} onChange={this.onChange} required/> 
                </section>

                <section className="register__container__content__article__section__2">
                    <h2>Wachtwoord</h2>
                    <input type="password" name="password" value={this.state.password} onChange={this.onChange} required/> 
                </section>

                <section className="register__container__content__article__section__2">
                    <h2>Bevestig wachtwoord</h2>
                    <input type="password" name="passwordConfirmation" value={this.state.passwordConfirmation} onChange={this.onChange} required/> 
                    <h3 id="js--passwordConfirmationError"></h3>
                </section>

                <section className="register__container__content__article__section__4">
                    <h2>E-mail</h2>
                    <input type="email" name="email" value={this.state.email} onChange={this.onChange} required/>
                    <h3 id="js--emailError"></h3>
                </section>

                <section className="register__container__content__article__section__5">
                    <h2>Adres</h2>
                    <input type="text" name="address" value={this.state.address} onChange={this.onChange} required/> 
                </section>

                <section className="register__container__content__article__section__6">
                    <h4>Op welke wijze wilt u gebruik gaan maken van deze app? Als:</h4>
                    <select className="" name="role" value={this.state.role} onChange={this.onChange} required>
                        <option value="" disabled>Kies een rol</option>
                        <option value="customer">Klant</option>
                        <option value="company">Bedrijf</option>
                        <option value="driver">Bezorger</option>
                    </select>
                    <h3 id="js--roleError"></h3>
                </section>
                
                <section className="register__container__content__article__section__7">
                    <button type="submit">   
                        <h4>Registreer</h4>
                    </button>
                </section>
            </form> 
        );
    }
}

export default RegisterForm;
