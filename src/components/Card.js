import React from 'react';
import axios from 'axios';

const BASE_URL = 'http://127.0.0.1:8000/api/auth/index';

class Card extends React.Component{
    state = {
        products: []
      }
    
    componentDidMount() {
        
        axios.get(BASE_URL)
          .then(res => {
            const products = res.data[0];
            console.log(products)
            this.setState({ products });
          })
      }

      render(){
            return(
                <section className="restaurant-grid">
                    { this.state.products.map(product => 
                                    
                        <article className="restaurant-grid__product-card">
                            <h1 className="restaurant-grid__product-card__product-name">{product.name}</h1>
                            <h2 className="restaurant-grid__product-card__product-price">{product.price}</h2>
                            <p className="restaurant-grid__product-card__product-description">{product.description}</p>
                        </article>
                    
                    )}
                </section>
                )
        }

}

export default Card;