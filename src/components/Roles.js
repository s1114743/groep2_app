import React from 'react';

import '../css/Register.css';

class Roles extends React.Component {

    render() {
        return (
            <section className="register__container__section">
                <h2>Op welke wijze wilt u gebruik gaan maken van deze app? Als:</h2>
                <section className="register__container__section__buttons">
                    <button>Klant</button>
                    <button>Bedrijf</button>
                    <button>Bezorger</button>
                </section>
            </section>
        );
    }
}

export default Roles;
