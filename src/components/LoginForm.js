import React from 'react';
import axios from 'axios';
import '../css/Login.css';

import { Link } from 'react-router-dom';

// form

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            remember_me: false
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onSubmit(e) {
        e.preventDefault();

        axios.defaults.xsrfCookieName = "csrftoken"; 
        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
    
        const user = {
            email: this.state.email,
            password: this.state.password,
            remember_me: this.state.remember_me
        };

        axios.post('http://localhost:8000/api/auth/login', user,
        {
            headers: {'Content-Type': 'application/json'}
        })
        .then(res => {
            console.log(res.data);
            // window.location = "/dashboard"     
        })
        .catch(error => {
            console.log(error.response.data.errors);
        });

    }

    render() {
        return (
            <form className="login__container__article" onSubmit={this.onSubmit}>
                    <section className="login__container__article__section__1">
                        <h2>E-mail</h2>
                        <input type="email" name="email" value={this.state.email} onChange={this.onChange} required/> 
                    </section>

                    <section className="login__container__article__section__2">
                        <h2>Wachtwoord</h2>
                        <input type="password" name="password" value={this.state.password} onChange={this.onChange} required/> 
                    </section>

                    <section className="login__container__article__section__3">

                    </section>

                    <section className="login__container__article__section__4">
                        <button>
                            <h4>Login</h4>
                        </button>
                    </section>
                    
                    <section className="login__container__article__section__5">
                        <h2>Nog geen account?</h2>
                            <Link to="/register" className="login__container__article__section__5__link">                      
                                <button>
                                    Registreer
                                </button> 
                            </Link>                     
                    </section>
                </form>
        );
    }
}

export default LoginForm;
