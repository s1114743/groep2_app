import React from 'react';
import "../css/Home.css"
import Nav from './Nav';

function Home(){
    return(

        <section>
            <Nav />
            <div className="container backgroundwrap">
                <h1 className="container__h1">Lekker wat voedsel bestellen</h1>
                <img className="backgroundwrap" alt=""></img>
            </div>
            <article className="zipcodeContainer">
                <input className="zipcodeContainer__input" placeholder="bv. Adres of postcode"></input>
            </article>
        </section>
    );
}

export default Home;
