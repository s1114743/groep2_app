import React from 'react';
import "../css/Restaurant.css";
import { Link, Route } from 'react-router-dom';
import Nav from "./Nav";
import routes from "./Routes";
import Addproduct from './Addproduct';
import Card from './Card'


const Restaurant = () =>{
         return(
            
            <section>
                <Nav />
                <section>
                    {routes.map(({ id }) =>(
                    <div key={id}>
                        <Link to={`/restaurant/${id}`}>
                    
                            <article className="restaurant-grid__add-product">
                                <div className="plusIcon">
                                    <ion-icon name="add-circle-outline"></ion-icon>
                                </div>
                                <h1>Voeg product toe</h1>
                            </article>

                        </Link> 
                    </div>
                ))}
                
                </section>
                <Route path={`/restaurant/:routesId`} component={Addproduct}/>
                
                        <Card />

            </section>
    );
}

export default Restaurant;
