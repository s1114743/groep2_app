import React from "react";
import "../css/Dashboard.css";
import Nav from "./Nav";
import { Link } from 'react-router-dom';

const Dashboard = () => {
  return (
    <article>
      <Nav />
      <section className="dashboard">
        <header className="dashboard__header">
          <h1>Dashboard</h1>
          <i className='fas fa-user'></i>
        </header>
        <article className="dashboard__filters">
          <p className="dashboard__filters__text">Type&#9660;</p>
        </article>
        
          
          <section className="dashboard__section">
            <Link to='/restaurant'>

            <span className="dashboard__section__circle">
              <div className="dashboard__section__circle__icon">
                <ion-icon name="restaurant-outline" size="large"></ion-icon>
              </div>
            </span>

            <p className="dashboard__section__text">De Knoempert</p>
            <p className="dashboard__section__text">Foto van een bonnetje</p>
            </Link>
          </section>
          
      </section>
    </article>
  );
}

export default Dashboard;
