import React from 'react';
import "../css/Nav.css";
import { Link } from 'react-router-dom';

function Nav(){
    return(
        <header className="nav__header">
            <nav>
                <div className="menuToggle">
                    <input type="checkbox" />
                    <span className="hamburgerMenu upper"></span>
                    <span className="hamburgerMenu middle"></span>
                    <span className="hamburgerMenu lower"></span>

                    <ul className="menu">
                        <li><Link to="/">Home</Link></li>
                        <li><Link to="/dashboard">Dashboard</Link></li>
                        <li><Link to="/login">Login</Link></li>
                        <li><Link to="/credits">Credits</Link></li>
                        <li><Link to="/register">Register</Link></li>
                        <li><Link to="/map">Map</Link></li>
                        <li><Link to="/thanks">Thanks</Link></li>

                    </ul>
                </div>
            </nav>
        </header>
    );


}

export default Nav;
