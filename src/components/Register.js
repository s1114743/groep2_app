import React from 'react';
import '../css/Register.css';
import RegisterForm from './RegisterForm';

const Register = () => {

    return (
        <section className="register__container">
        <header className="register__header">
            <h1>Registreren</h1>
        </header>
            <RegisterForm />
        </section>
    );
}

export default Register;
