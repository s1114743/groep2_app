import React from "react";
import Map from './ThanksMap';
import "../css/Thanks.css";

const Thanks = (props) => {
  return (
    <article className="confirmation">
      <header className="confirmation__header">
        <h1>Bedankt voor de aankoop!</h1>
      </header>

      <section className="confirmation__map">
        <Map />
      </section>

      <h2 className="confirmation__questions">Wat vond u van onze service?</h2>

      <article className="confirmation__questions__choices">
        <button className="confirmation__questions__choices__text">Slecht</button>
        <button className="confirmation__questions__choices__text">Matig</button>
        <button className="confirmation__questions__choices__text">Goed</button>
      </article>

      <textarea className="confirmation__input" placeholder="Verdere feedback..."></textarea>
      <button className="confirmation__button">Opsturen</button>
    </article>
  );
}

export default Thanks;
