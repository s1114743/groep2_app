import React from 'react';

import L from 'leaflet';

import 'leaflet/dist/leaflet.css';

import styled from 'styled-components';

import 'leaflet-routing-machine';

import '../css/Map.css';

import Nav from "./Nav";

// map container aanmaken
const Wrapper = styled.div`
    width: ${props => props.width};
    height: ${props => props.height};
`;

// markers aanmaken
const locationMarker = L.icon({
    iconUrl: 'images/location.png',
    iconSize:     [20, 20],                                     
    shadowSize:   [50, 64],                                     
    iconAnchor:   [11, 20],                                     
    shadowAnchor: [4, 62],                                      
    popupAnchor:  [-3, -76]                                     
});

const startMarker = L.icon({
    iconUrl: 'images/start.png',
    iconSize:     [25, 25],                                     
    shadowSize:   [50, 64],                                     
    iconAnchor:   [11, 20],                                     
    shadowAnchor: [4, 62],                                      
    popupAnchor:  [-3, -76]                                     
});

const destinationMarker = L.icon({
    iconUrl: 'images/destination.png',
    iconSize:     [30, 30],                                     
    shadowSize:   [50, 64],                                     
    iconAnchor:   [11, 20],                                     
    shadowAnchor: [4, 62],                                      
    popupAnchor:  [-3, -76]                                     
});

export default class Map extends React.Component {

    // overkoepelende map functie
    componentDidMount() {

        // map aanmaken
        this.map = L.map('map', {
            minZoom: 14,
            maxZoom: 18
        }).setView([52.131097, 4.645999], 13);

        // map style toevoegen aan map
        L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_labels_under/{z}/{x}/{y}{r}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        
        // array met het start adres en bestemming adres van een bestelling
        let addresses = ['Hooftstraat 81 Alphen aan den Rijn', 'Portugalstraat 27 Alphen aan den Rijn'];

        // geolocation api BASE_URL
        const BASE_URL = 'http://open.mapquestapi.com/geocoding/v1/address?key=75LC00e2SkzibcA1H9kSl6LtOnrokBZK&location=';
   
        // // het start adres en bestemming adres opslaan in variabelen en terug geven 
        async function getAdresses() {
            const startAddress = await fetch(BASE_URL + addresses[0]).then(data => data.json()).then(data => data.results[0].locations[0].latLng);
            const destinationAddress = await fetch(BASE_URL + addresses[1]).then(data => data.json()).then(data => data.results[0].locations[0].latLng);

            return [startAddress, destinationAddress];
        }

        // route aanmaken tussen start adres en bestemming adres
        const createRoute = async () => {
            const route = await getAdresses();

            L.Routing.control({
                waypoints: [
                    L.latLng(route[0]),
                    L.latLng(route[1])
                ],
                show: true,                                                                 
                addWaypoints: false,                                                        
                draggableWaypoints: false,                                                  
                createMarker: function(i, wp, nWps) {
                    if (i === 0) {
                        return L.marker(wp.latLng, {icon: startMarker });
                    } else if (i === nWps -1) {
                        return L.marker(wp.latLng, {icon: destinationMarker });
                    } else {
                        return L.marker(wp.latLng, {icon: locationMarker });
                    }
                }
            }).addTo(this.map);
        }

        // live loccatie bijhouden van gebruiker
        this.map.locate({
            watch: true,
            setView: false,
            maxZoom: 16
        }).on('locationfound', (e) => {
            if (!this.marker) {
                this.marker = L.marker([e.latitude, e.longitude], { icon: locationMarker }).addTo(this.map);
            } else {
                this.marker.setLatLng([e.latitude, e.longitude]);
            }  
        });
        
        createRoute();
    }

    render() {
        return (
            <section>
                <Nav />
                <Wrapper className="bigMap" width="100%" height="95vh" id="map" />
            </section>
        )
    }

}