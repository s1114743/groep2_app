import React from 'react'
import Nav from './Nav';
import '../css/Credits.css'
import axios from 'axios';

class Credits extends React.Component {

    state = {
        creditsAmount: null,
    }

    handleChange = event => {
        this.setState({ creditsAmount: event.target.value });
    }

    handleAddCreditsToAccount = (event) => {
        event.preventDefault();

        if(this.state.creditsAmount !== null) {
            axios.defaults.xsrfCookieName = "csrftoken";
            axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";

            const userAndCredits = {
                name: 'tim',
                credits: this.state.creditsAmount,
            }
            
            const BASE_URL = `http://localhost:8000/api/credits/increment/${userAndCredits.name}`;

            axios.put(BASE_URL, userAndCredits, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest',
                },
            })
            .then(res => {
                console.log(res);
                console.log(userAndCredits.name);
                console.log(userAndCredits.credits);
            })
        }
    }

    render() {
        return (
            <section className="credits">
                <Nav />
                <form className="form__credits" name="formCredits">
                    <section className="form__credits__section">
                        <input type="radio" id="500" name="credit" value="500" className="form__credits__input" onChange={this.handleChange} />
                        <label htmlFor="500" className="form__credits__label">500</label>

                        <input type="radio" id="1000" name="credit" value="1000" className="form__credits__input" onChange={this.handleChange} />
                        <label htmlFor="1000" className="form__credits__label">1000</label>
 
                        <input type="radio" id="2000" name="credit" value="2000" className="form__credits__input" onChange={this.handleChange} />
                        <label htmlFor="2000" className="form__credits__label">2000</label>
                        
                        <input type="radio" id="5000" name="credit" value="5000" className="form__credits__input" onChange={this.handleChange} />
                        <label htmlFor="5000" className="form__credits__label">5000</label>
                    </section>
                    <article className="form__credits__button">
                        <button type="submit" className="form__credits__submit" onClick={this.handleAddCreditsToAccount}>Voeg Toe</button>
                    </article>
                </form>
            </section>
        );
    }
}

export default Credits;
