import React from "react";
import "../css/Transaction.css";

class Transaction extends React.Component{
  render(){
    return(
      <article>
        <section className="shoppingcart_section">
          <button className="shoppingcart_section_button">winkelwagen</button>
          <div className="seperation_line"></div>
        </section>

        <section className="inputfields_section">
          <form action="POST">
            <section className="address_input">
              <label >
                Straatnaam en huisnummer
                <input type="text" name="Streetname & Housenumber" id="street"/>
              </label>

              <label >
                Postcode
                <input type="text" name="Postal Code" id="postalcode"/>
              </label>

              <label>
                Plaatsnaam
                <input type="text" name="City Name" id="city"/>
              </label>

            </section>

            <div className="seperation_line"></div>

            <section className="contact_info">
              <label >
                naam
                <input type="text" name="Name" id="name"/>
              </label>

              <label >
                E-mail
                <input type="text" name="E-mail" id="email"/>
              </label>

              <label >
                Telefoonnummer
                <input type="text" name="Phonenumber" id="phone" />
              </label>
            </section>

            <div className="seperation_line"></div>

            <section className="bank_details">
              <button className="bank_details_dropdown_menu">Kies je bank</button>
            </section>

            <section>
              <button className="order_button" type="submit">bestel</button>
            </section>
          </form>
        </section>
      </article>
    )
  }
}

export default Transaction;
