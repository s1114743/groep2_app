import React from 'react';
import '../css/header.css';

const Header = ({title}) => {
    return (
        <header className="header">
            <h1 className="header__heading-primary">{title || "Geen Titel"}</h1>
        </header>
    );
}

export default Header;
