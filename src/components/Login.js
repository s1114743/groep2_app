import React from 'react';

import '../css/Login.css';
import '../css/Login.css';

import LoginForm from './LoginForm'

const Login = () => {
    return (
        <section className="login__container">
            <header className="login__header">
                <h1>Inloggen</h1>
            </header>
            <LoginForm />
        </section>
    );
}

export default Login;
