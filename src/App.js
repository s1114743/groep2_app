import React from 'react';
import Home from './components/Home';
import Dashboard from './components/Dashboard';
import Restaurant from './components/Restaurant';
import Thanks from './components/Thanks';
import Login from './components/Login';
import Register from './components/Register'
import Map from './components/Map'
import Credits from './components/Credits';
import Roles from './components/Roles'

import './css/App.css';

import { BrowserRouter, Route, Switch } from 'react-router-dom';

const App = () => {
  return (
    <section>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/thanks" component={Thanks} />
          <Route path="/dashboard" component={Dashboard} />
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/map" component={Map} />
          <Route path="/restaurant" component={Restaurant} />
          <Route path="/credits" component={Credits} />
          
          {/* <Route path="/transaction" component={Transaction} /> */}
          <Route path="/roles" component={Roles} />
        </Switch>
      </BrowserRouter>
    </section>
  );
}

export default App;
